﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Prueba.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.DataAccess
{
    public class PruebaDbContext : IdentityDbContext<ApplicationUser>
    {
        public PruebaDbContext(DbContextOptions<PruebaDbContext> options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region ------------- IdentificationTypes ----------------
            modelBuilder.Entity<IdentificationType>().HasData(
                  new IdentificationType
                  {
                      Id = 1,
                      Name = "CC"

                  },
                   new IdentificationType
                   {
                       Id = 2,
                       Name = "RC"

                   },
                    new IdentificationType
                    {
                        Id = 3,
                        Name = "TI"

                    },
                     new IdentificationType
                     {
                         Id = 4,
                         Name = "CE"

                     },
                      new IdentificationType
                      {
                          Id = 5,
                          Name = "PA"

                      }
              );
            #endregion

        }
    }
}
