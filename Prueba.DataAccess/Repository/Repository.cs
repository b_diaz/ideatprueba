﻿using System;
using System.Collections.Generic;
using System.Text;
using Prueba.Models;


namespace Prueba.DataAccess
{
    /// <summary>
    /// Repository that implements the CRUD operations
    /// </summary>
    /// <typeparam name="T">Model class where the CRUD operations will be executed</typeparam>
    /// <typeparam name="Tid">Class Identificator type</typeparam>
    public class Repository<T, TId> : Base.RepositoryWithTypedId<T, TId>, IRepository<T, TId>
      where T : class, Models.Base.IEntityWithTypedId<TId>
    {
        public Repository(PruebaDbContext context) : base(context)
        {

        }
    }
}
