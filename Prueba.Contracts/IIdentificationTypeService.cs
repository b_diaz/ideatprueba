﻿using System;
using Prueba.Models;

namespace Prueba.Contracts
{
    public interface IIdentificationTypeService
    {
        IdentificationType GetById(int IdentificationTypeId);
    }
}
