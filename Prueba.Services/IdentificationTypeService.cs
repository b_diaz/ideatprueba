﻿using System;
using System.Collections.Generic;
using System.Text;
using Prueba.Contracts;
using Prueba.Models;
using Prueba.DataAccess;
using System.Linq;



namespace Prueba.Services
{
    public class IdentificationTypeService : IIdentificationTypeService
    {
        #region --------- Dependencias ------------

        private readonly IRepository<IdentificationType, int> _IdentificationTypeRepository;

        public IdentificationTypeService
            (IRepository<IdentificationType, int> IdentificationTypeRepository)
        {
            this._IdentificationTypeRepository = IdentificationTypeRepository;
        }
        #endregion

        public IdentificationType GetById(int IdentificationTypeId)
        {
            try
            {
                var result = _IdentificationTypeRepository.Query()
               .Where(x => x.Id == IdentificationTypeId)
               .FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
