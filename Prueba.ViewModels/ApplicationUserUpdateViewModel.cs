﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.ViewModels
{
    public class ApplicationUserUpdateViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int IdentificationTypeId { get; set; }
        public int IdentificationNumber { get; set; }
        public string Email { get; set; }
    }
}
