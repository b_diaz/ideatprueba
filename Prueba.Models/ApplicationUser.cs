﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int IdentificationTypeId { get; set; }
        public IdentificationType IdentificationType { get; set; }
        public IdentityRole Roles { get; set; }
        public int IdentificationNumber { get; set; }

    }
}
