﻿using Prueba.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.Models
{
    public class IdentificationType : EntityBase<int>
    {
        public string Name { get; set; }
    }
}
